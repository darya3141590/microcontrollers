import time

import paho.mqtt.client as paho
import questionary

broker = 'broker.emqx.io'

client = paho.Client("client-isu-Dasha")

print("Connecting to broker ", broker)
client.connect(broker)
client.loop_start()
print("Publishing")

topic = questionary.text("Your topic: ").ask()

start = 20
minimum = 10
duration = start

while True:
    print("command is u")
    client.publish(topic, "u")
    time.sleep(20)

    print("command is d")
    client.publish(topic, "d")
    time.sleep(duration)

    print("command is u")
    client.publish(topic, "u")
    time.sleep(60 - (20 + duration))

    if duration > minimum:
        duration -= 1
    else:
        duration = start

