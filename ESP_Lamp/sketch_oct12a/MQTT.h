#include <PubSubClient.h>

PubSubClient mqtt_client(wifi_client); 

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message received on topic: ");
  Serial.println(topic);
  Serial.print("Message is: ");
  Serial.println((char)payload[0]);
  if ((char)payload[0] == 'u') {
    digitalWrite(led_pin, HIGH);
  } else {
    digitalWrite(led_pin, LOW);
  }
}
bool init_MQTT() {
  mqtt_client.setServer(mqtt_broker, mqtt_port);
  mqtt_client.setCallback(callback);
  while(!mqtt_client.connected()) {
    Serial.print("Trying to connect: ");
    Serial.println(mqtt_broker);
    String client_id = "esp8266_" + id();
    bool success = mqtt_client.connect(client_id.c_str());
    if (success) {
      Serial.println("Successfully connected with " + client_id);
    } else {
      Serial.println("Failed to connect with " + client_id);
      Serial.println(mqtt_client.state());
      delay(2000);
    }
  }
  mqtt_client.subscribe(topic.c_str());
  return true;
}