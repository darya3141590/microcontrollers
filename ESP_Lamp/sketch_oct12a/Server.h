#include <ESP8266WebServer.h>

ESP8266WebServer server(web_port);

void handle_root() {
  String page_code = "<form action=\"WEB\" method=\"post\"><input type=\"text\" name=\"ssid\" placeholder=\"SSID\"><input type=\"text\" name=\"pass\" placeholder=\"Password\"><input type=\"submit\" value=\"Submit\"></form>";

  server.send(200, "text/html", page_code);
}

void handle_led() {
  bool current = digitalRead(led_pin);
  digitalWrite(led_pin, !current);
  server.sendHeader("Location", "/");
  server.send(303);
}

void handle_web() {
  String ssid = server.arg("ssid");
  String pass = server.arg("pass");
  Serial.println("SSID: " + ssid);
  Serial.println("Password: " + pass);
  bool connected = init_WIFI(false, ssid, pass);
  if (connected) {
    init_MQTT();
    server.send(200, "text/plain", "working");
  } else {
    server.send(400, "text/plain", "isn't working");
  }
}


void handle_not_found() {
  server.send(404, "text/html", "404: check URL");
}

void server_init() {
  server.on("/", HTTP_GET, handle_root);
  server.on("/WEB", HTTP_POST, handle_web);
  server.onNotFound(handle_not_found);
  server.begin();
  Serial.println("HTTP Server is on, on port " + String(web_port));
}