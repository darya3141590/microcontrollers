#include "Config.h" 
#include "WIFI.h"
#include "MQTT.h"
#include "Server.h"

void setup() {
  Serial .begin(9600);
  pinMode(led_pin, OUTPUT);
  init_WIFI(true, "", ""); 
  server_init(); 
}

void loop() {
  server . handleClient();
  mqtt_client.loop();
  delay(100);
}
