int led1_pin = 2;
int led2_pin = 3;
int led3_pin = 4;
int led4_pin = 5;

void setup() {
  pinMode(led1_pin, OUTPUT);
  pinMode(led2_pin, OUTPUT);
  pinMode(led3_pin, OUTPUT);
  pinMode(led4_pin, OUTPUT);
}

void loop() {
  // свет от 1
  digitalWrite(led1_pin, HIGH);
  digitalWrite(led2_pin, LOW);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, HIGH);
  delay(500);
  // свет от 2
  digitalWrite(led1_pin, HIGH);
  digitalWrite(led2_pin, LOW);
  digitalWrite(led3_pin, HIGH);
  digitalWrite(led4_pin, LOW);
    delay(500);
  // свет от 3
  digitalWrite(led1_pin, LOW);
  digitalWrite(led2_pin, HIGH);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, HIGH);
   delay(500);
  // свет от 4
  digitalWrite(led1_pin, LOW);
  digitalWrite(led2_pin, HIGH);
  digitalWrite(led3_pin, HIGH);
  digitalWrite(led4_pin, LOW);
  delay(500);


  // свет от  12
  digitalWrite(led1_pin, HIGH);
  digitalWrite(led2_pin, LOW);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, LOW);
  delay(1000);
  //13
  digitalWrite(led1_pin, HIGH);
  digitalWrite(led2_pin, HIGH);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, HIGH);
  delay(1000);
   // свет от 14
  for (int i = 0; i < 1000; i++) {
    // свет от 1
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, LOW);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(1);
    // свет от 4
    digitalWrite(led1_pin, LOW);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, HIGH);
    digitalWrite(led4_pin, LOW);
    delay(1);
  }


  // свет от 23
  for (int i = 0; i < 1000; i++) {
    // свет от 2
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, LOW);
    digitalWrite(led3_pin, HIGH);
    digitalWrite(led4_pin, LOW);
    delay(1);
    // свет от 3
    digitalWrite(led1_pin, LOW);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(1);
  }
  // свет от 24
  digitalWrite(led1_pin, HIGH);
  digitalWrite(led2_pin, HIGH);
  digitalWrite(led3_pin, HIGH);
  digitalWrite(led4_pin, LOW);
  delay(1000);

  // свет от 34
  digitalWrite(led1_pin, LOW);
  digitalWrite(led2_pin, HIGH);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, LOW);
  delay(1000);

  /// свет от 123
  for (int i = 0; i < 1000; i++) {
    // свет от 21
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, LOW);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, LOW);
    delay(1);
    // свет от 3
    digitalWrite(led1_pin, LOW);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(1);
  }
  // свет от 124 (!!! 3 лампа)
  for (int i = 0; i < 1000; i++) {
    // свет от 24
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, HIGH);
    digitalWrite(led4_pin, LOW);
    delay(10);
    // свет от 1
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, LOW);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(10);
  }
  // свет от 134
  for (int i = 0; i < 1000; i++) {
    //123
    //34
    digitalWrite(led1_pin, LOW);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, LOW);
    delay(1);
    //1
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, LOW);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(1);
  }
  //  свет от 234
  for (int i = 0; i < 1000; i++) {
    // свет от 24
    digitalWrite(led1_pin, HIGH);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, HIGH);
    digitalWrite(led4_pin, LOW);
    delay(1);
    // свет от 3
    digitalWrite(led1_pin, LOW);
    digitalWrite(led2_pin, HIGH);
    digitalWrite(led3_pin, LOW);
    digitalWrite(led4_pin, HIGH);
    delay(1);
  }
}